// 1. Create an a1 folder and inside create an activity.js file to write and save the solution for the activity.

// 2. Use the count operator to count the total number of fruits on sale.

// Solution1
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: null,
        totalFruitsOnSale: {
            $count: {}
        }
    }},
    {
        $project: {
          _id: 0
        }
    }    
]);

// Solution2
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: null,
        totalFruitsOnSale: {
            $sum: "$stock"
        }
    }},
    {
        $project: {
          _id: 0
        }
    }    
]);

// 3. Use the count operator to count the total number of fruits with stock more than 20.

// Solution1
db.fruits.aggregate([
    {
        $match: {
        onSale:true,
        stock: {$gt: 20}
    }},
    {
        $group: {
        _id: null,
        totalFruitsGT20: {
            $count: {}
        }
    }},
    {
        $project: {
          _id: 0
        }
    } 
]);

// Solution2
db.fruits.aggregate([
    {
        $match: {
        onSale:true,
        stock: {$gt: 20}
    }},
    {
        $group: {
        _id: null,
        totalFruitsGT20: {
            $sum: "$stock"
        }
    }},
    {
        $project: {
          _id: 0
        }
    }    
]);

// Solution3
db.fruits.aggregate([
    {
        $match: {
        onSale:true,
        stock: {$gt: 20}
    }},
    {
        $group: {
        _id: "$name",
        totalFruitsGT20: {
            $count: {}}
        }
    },
    {
        $project: {
          _id: 0
        }
    }    
]);
// 4. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
            _id: "$supplier_id",
        averagePrice: {
            $avg: "$price"
        }
    }} 
]);
// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: "$supplier_id",
        maxPrice: {
            $max: "$price"
        }
    }}
]);
// 6. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: "$supplier_id",
        minPrice: {
            $min: "$price"
        }
    }}
]);
// 7. Create a gitlab project repository named a1 in s25.

// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

// 9. Add the link in Boodle.